let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0

nmap <leader>ah :ALEHover<CR>
nmap <leader>afr :ALEFindReferences<CR>
nmap <leader>afx :ALEFix<CR>
nmap <leader>arn :ALERename<CR>
nmap <leader>ass :ALESymbolSearch<CR>
nmap <leader>ad :ALEDetail<CR>
