let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

nmap <C-b> :CtrlPBuffer<CR>

let g:ctrlp_prompt_mappings = { 'PrtDeleteEnt()': ['<C-l>'] }
