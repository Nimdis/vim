let g:hardtime_default_on = 0
let g:hardtime_ignore_buffer_patterns = ["NERD.*"]
let g:hardtime_allow_different_key = 1
