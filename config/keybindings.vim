"key mapping for window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"key mapping for tab navigation
nmap <Tab> gt
nmap <S-Tab> gT

"Key mapping for easy splits
"nmap <leader>j :vsp<CR>
"nmap <leader>f :sp<CR>

"Paste no paste
nmap <leader>y :set paste<CR>
nmap <leader>Y :set nopaste<CR>

nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a

"buffers
nmap g] :bn<CR>
nmap g[ :bp<CR>
nmap <leader>bn :bn<CR>
nmap <leader>bp :bp<CR>
nmap <leader>bd :bd<CR>
nmap <leader>q :q<CR>
nmap <leader>s :w<CR>
