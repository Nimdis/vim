" NERDTree settings
nmap wm :NERDTree<cr>
let NERDTreeIgnore=['\.sw*$']

"let nerdtree veiw hidden files
let NERDTreeShowHidden=1
silent! nmap <silent> <Leader>p :NERDTreeToggle<CR>
