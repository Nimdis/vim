call plug#begin('~/.vim/plugged')

" Utils
Plug 'jremmen/vim-ripgrep'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'
Plug 'sjl/gundo.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'easymotion/vim-easymotion'
Plug 'yangmillstheory/vim-snipe'
Plug 'scrooloose/nerdtree'
Plug 'mhinz/vim-startify'
Plug 'terryma/vim-multiple-cursors'
Plug 'editorconfig/editorconfig-vim'
Plug 'andymass/vim-matchup'
Plug 'dyng/ctrlsf.vim'
Plug 'vimwiki/vimwiki', { 'branch': 'dev' }
Plug 'Yggdroot/indentLine'


" Complete and lint
"Plug 'scrooloose/syntastic'
"Plug 'Valloric/YouCompleteMe'
"if has('nvim')
  "Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"else
  "Plug 'Shougo/deoplete.nvim'
  "Plug 'roxma/nvim-yarp'
  "Plug 'roxma/vim-hug-neovim-rpc'
"endif
"let g:deoplete#enable_at_startup = 1
"Plug 'ervandew/supertab'

" GIT
Plug 'tpope/vim-fugitive'
Plug 'itchyny/vim-gitbranch'
"Plug 'airblade/vim-gitgutter'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-git'
Plug 'mhinz/vim-signify'

" Syntax and langs
Plug 'sheerun/vim-polyglot'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'dense-analysis/ale'
"Plug 'Quramy/tsuquyomi'
"Plug 'prettier/vim-prettier'
Plug 'tpope/vim-rails', { 'for': 'ruby' }
"Plug 'python-mode/python-mode', { 'branch': 'develop' }
Plug 'chrisbra/csv.vim', { 'for': 'csv' }
"Plug 'styled-components/vim-styled-components'
Plug 'hail2u/vim-css3-syntax'
Plug 'pangloss/vim-javascript'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'MaxMEllon/vim-jsx-pretty'
" ts syntax it updated more frequent than polyglot
Plug 'HerringtonDarkholme/yats.vim'

"colors
Plug 'rafi/awesome-vim-colorschemes'
Plug 'mhartington/oceanic-next'


call plug#end()

