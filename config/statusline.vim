set modeline
set modelines=5
set ruler
set laststatus=2
" TODO filepath, has changed, filetype, encoding, file %, currentline/total, current character
set statusline=
set statusline+=%< " truncate point
set statusline+=\ %f " filepath
set statusline+=\ %y " filetype
set statusline+=\ [%{&ff}] " encoding
set statusline+=\ %m " modifier [+] [-]
set statusline+=\ %r " readonly
set statusline+=[%{gitbranch#name()}] " git branch name
set statusline+=%= " right align
set statusline+=Line:\ %l\ /%L " current position in lines
set statusline+=\ %p%% " percents
" current position in characters
set statusline+=\ Pos:\ %c\ "

