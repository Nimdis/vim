let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['eslint']
let syntastic_mode_map = { 'passive_filetypes': ['html'] }
nmap <leader>e :SyntasticCheck eslint<CR>
nmap <leader>et :SyntasticCheck tslint<CR>
