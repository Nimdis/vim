"source ~/.vim/config/vundle.vim
source ~/.vim/config/plug.vim
source ~/.vim/config/general.vim
source ~/.vim/config/keybindings.vim
source ~/.vim/config/nerdtree.vim
source ~/.vim/config/gundo.vim
source ~/.vim/config/striptrailingwhitespaces.vim
source ~/.vim/config/statusline.vim
source ~/.vim/config/syntastic.vim
source ~/.vim/config/hardtime.vim
source ~/.vim/config/ctrlp.vim
source ~/.vim/config/overlength.vim
source ~/.vim/config/easymotion.vim
source ~/.vim/config/coc.vim
source ~/.vim/config/ale.vim

"let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
"let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
"set term=screen-256color
set t_ut=

" fix for tmux
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
